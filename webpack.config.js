var path = require('path'),
    node_modules_dir = path.resolve(__dirname, 'node_modules'),
    webpack = require('webpack');

var config = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: 'bundle.js'
  },
  devtools: "source-map",
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: [node_modules_dir],
      loader: 'babel'
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};

module.exports = config;
