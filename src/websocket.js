let socket,
    messagesToIgnore = 0,
    messageBundleIds = [],
    messageHandler;

let $presenterRequest = {
    contentType: "application/json",
    transport: "websocket",
    fallbackTransport: "websocket",
    trackMessageLength: true,
    reconnectInterval: -1,
    maxReconnectOnClose: -1,
    logLevel: "debug",
    enableProtocol: false,
    suspend: false,
    timeout: 20000,
    closeAsync: true,
    headers: {'Api-Version': '03'},
    onOpen: function() {},
    onMessage: function(response) {
        var message;
        if(messagesToIgnore < 1)
        {
            message = JSON.parse(response.responseBody);
            switch(message.type)
            {
                case 'SOCKET_OPEN':
                    console.log("Opened");
                    break;
                case 'SOCKET_DENIED':
                    console.log("Socket Denied");
                    console.log(message);
                    break;
                case 'FAILED_ACK':
                    //TODO
                    break;
                case 'ACK':
                    handleAck(message);
                    break;
                default:
                    messageHandler(message);
                    break;
            }
        }
        else
        {
            messagesToIgnore--;
        }
    },
    onError: function() {
        console.log("OnError");
    },
    onTransportFailure: function() {
        console.log("OnTransportFailure");
    },
    onClientTimeout: function() {
        console.log("OnClientTimeout");
    },
    onClose: function() {
        console.log("OnClose");
    },
    onReconnect: function() {
        console.log("OnReconnect");
    }
};

function connectToSession(connectionInfo)
{
    $presenterRequest.url = connectionInfo;
    socket = window.jQuery.atmosphere.subscribe($presenterRequest);
    heartbeat.start();
}

function onMessage(func)
{
  messageHandler = func;
}

function handleAck(message)
{
    if(messageBundleIds.indexOf(message.id) == -1)
    {
        messageBundleIds.push(message.id);
    }
    else
    {
        messagesToIgnore = message.numberOfMessages;
    }

    window.jQuery.atmosphere.requests[0].push(JSON.stringify(message));
}

//Mini-module for handling heartbeats.
let heartbeat = (function heartbeat() {
    var heartBeat,
        heartbeatJSON = {};

    heartbeatJSON.type = 'HEARTBEAT';

    function beat()
    {
        socket.push(JSON.stringify(heartbeatJSON));
    }

    function start()
    {
        beat();
        heartBeat = setInterval(beat, 15000);
    }

    function end()
    {
        clearTimeout(heartBeat);
    }

    return {
        start:start,
        end:end
    };
}());

export { connectToSession, onMessage }
